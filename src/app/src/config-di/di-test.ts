import {container} from "./di-container";

import {DI_TYPES} from "./di-types";

import {ArtifactRESTServiceInterface} from "../definitions/ldt-api/artifacts/service/ArtifactRESTService";
import {ArtifactMockRESTService} from "../module/artifact/api/ArtifactMockRESTService";
import {ArtifactRepository} from "../module/artifact/repository/ArtifactRepository";

container.bind<ArtifactRESTServiceInterface>(DI_TYPES.ArtifactRESTService)
    .to(ArtifactMockRESTService)
    .inSingletonScope();

container.bind<ArtifactRepository>(ArtifactRepository)
    .to(ArtifactRepository)
    .inSingletonScope();