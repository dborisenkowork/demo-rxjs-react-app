import {container} from "./di-container";
import {interfaces} from "inversify";
import Context = interfaces.Context;

import {ArtifactRESTService} from "../definitions/ldt-api/artifacts/service/ArtifactRESTService";
import {ReactRESTAdapter} from "../module/common/services/ReactRESTAdapter";

container.bind<ArtifactRESTService>(ArtifactRESTService)
    .toFactory<ArtifactRESTService>((context: Context) => {
        return () => {
            return new ArtifactRESTService(context.container.get(ReactRESTAdapter));
        };
    });