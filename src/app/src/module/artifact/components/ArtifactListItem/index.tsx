import * as React from "react";

import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Subject} from "rxjs/Subject";

import {Artifact} from "../../../../definitions/ldt-api/artifacts/entity/ArtifactEntity";

export interface ArtifactsListItemProps
{
    artifact$: BehaviorSubject<Artifact>,
}

interface ArtifactsListItemState
{
    artifact: Artifact,
}

export class ArtifactListItem extends React.Component<ArtifactsListItemProps, ArtifactsListItemState>
{
    private unmount: Subject<void> = new Subject<void>();

    componentWillMount(): void {
        this.props.artifact$
            .takeUntil(this.unmount)
            .subscribe(a => this.setState({
                artifact: a
            }));
    }

    componentWillUnmount(): void {
        this.unmount.next(undefined);
    }

    render(): JSX.Element {
        return (
            <div>
                <h1>{this.state.artifact.entity.code}</h1>
                <h2>{this.state.artifact.entity.title}</h2>
                <p>{this.state.artifact.entity.description}</p>
                <p>{this.state.artifact.entity.updating}</p>
            </div>
        );
    }
}