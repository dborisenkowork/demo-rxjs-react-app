import * as React from "react";

import "./style.less";

import {Subject} from "rxjs/Subject";
import {FormattedMessage} from "react-intl";

import {lazyInject} from "../../../../config-di/di-common";
import {UINavigationService} from "../../services/UINavigationService";


interface UITitleState
{
    current: { id: string, defaultMessage: string }[],
}

export class UITitle extends React.Component<undefined, UITitleState>
{
    @lazyInject(UINavigationService) private uiNavigation: UINavigationService;

    private unmount: Subject<void> = new Subject<void>();

    constructor() {
        super();

        this.state = {
            current: [],
        };
    }

    componentDidMount(): void {
        this.uiNavigation.onChanges
            .takeUntil(this.unmount)
            .subscribe(next => {
                this.setState({
                    current: next
                        .filter(p => p.displayInNavbar)
                        .map(p => {
                            return {
                                id: p.title.formattedMessage.id,
                                defaultMessage: p.title.formattedMessage.defaultMessage
                            }
                        })
                })
            });
    }

    componentWillUnmount(): void {
        this.unmount.next(undefined);
    }

    render(): JSX.Element {
        let items = this.state.current.map((p, index) => (
            <span className="ldt___ui-title_section" key={index}>
                <FormattedMessage id={p.id} defaultMessage={p.defaultMessage}/>
            </span>
        ));

        return (
            <div className="ldt___ui-title title is-5">
                <div className="ldt__ui-title_container">
                    <span className="ldt___ui-title_section">
                        <FormattedMessage id="ui.title.main" defaultMessage="LDT"/>
                    </span>
                    {items}
                </div>
            </div>
        );
    }
}