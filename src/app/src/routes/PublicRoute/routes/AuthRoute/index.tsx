import * as React from "react";

import {lazyInject} from "../../../../config-di/di-common";
import {UINavigationService} from "../../../../module/ui/services/UINavigationService";

export class PublicAuthRoute extends React.Component<undefined, undefined>
{
    @lazyInject(UINavigationService) private uiNavigation: UINavigationService;

    componentWillMount(): void {
        this.uiNavigation.push({
            displayInNavbar: true,
            title: {
                formattedMessage: {
                    id: 'routes.public.auth.title',
                    defaultMessage: 'Auth'
                }
            }
        });
    }

    componentWillUnmount(): void {
        this.uiNavigation.pop();
    }

    render(): JSX.Element {
        return (
            <div>
                <h1>Auth</h1>
            </div>
        );
    }
}