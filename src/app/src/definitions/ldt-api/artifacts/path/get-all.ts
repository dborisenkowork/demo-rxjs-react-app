import {Artifact} from "../entity/ArtifactEntity";

export const urlArtifactsGetAll = '/api/v1.0/artifacts/all';

export interface ArtifactsGetAllResponse200
{
    success: boolean,
    artifacts: Artifact[],
}