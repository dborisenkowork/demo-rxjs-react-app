import * as React from "react";
import * as ReactDOM from "react-dom";

import "./config/polyfills";
import "./config/vendor";
import "./config/i18n";

import "./config-di/di-common";
import "./config-di/di-test";

import "./styles/index.less";

import {IntlProvider} from 'react-intl';
import {LDT} from "./module";

const i18nLocale = 'ru';
const i18nMessages = require('./translations/ru_RU.json');

ReactDOM.render(
    <IntlProvider locale={i18nLocale} messages={i18nMessages}>
        <LDT.Modules.UI.Components.UIApp/>
    </IntlProvider>,
    document.getElementById("reactUIAppRoot")
);